import "phaser";
import TextConfig from '../../Config/text_config';
import ScoreConfig from '../../Config/score_config';

export default class ScoreManager {
    /*
        ScoreManager class to manage score increments, additions, and update score text
    */
    private static instance: ScoreManager;
    private score: number;
    private text: Phaser.GameObjects.Text;
    private stepTimer: Phaser.Time.TimerEvent;
    
    private constructor() {
        this.reset();
    }

    reset(): void {
        this.setScore(0);
    }

    setScore(pts: number): void {
        this.score = pts;
        this.updateText();
    }

    addScore(pts: number): void {
        this.setScore(this.score + pts);
    }

    getScore(): number {
        return this.score;
    }

    setText(object: Phaser.GameObjects.Text): void {
        this.text = object;
        this.reset();
    }

    updateText(): void {
        if (this.text)
        {
            this.text.setText(this.score + '');
            this.text.x = TextConfig.score.position.x - this.text.width / 2;
            this.text.y = TextConfig.score.position.y - this.text.height / 2;
        }
    }

    start(scene: Phaser.Scene): void {
        this.stepTimer = scene.time.addEvent({
            delay: ScoreConfig.stepDelay,
            callback: function () {
                this.addScore(ScoreConfig.step)
            }.bind(this),
            loop: true
        })
    };

    stop(): void {
        this.stepTimer.remove();
    }

    static getInstance(): ScoreManager {
        if (!ScoreManager.instance)
        {
            ScoreManager.instance = new ScoreManager();
        }
        return ScoreManager.instance;
    }
}