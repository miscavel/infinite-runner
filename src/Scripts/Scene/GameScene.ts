import "phaser";
import { GameSceneKey } from '../../Config/scene_config';
import { ParallaxSpriteConfig, TileSpriteConfig } from '../../Config/sprite_config';
import { InteractiveType } from '../../Config/interactive_config';
import ParallaxConfig from '../../Config/parallax_config';
import DepthConfig from '../../Config/depth_config';
import TextConfig from '../../Config/text_config';
import ScoreConfig from '../../Config/score_config';
import AudioConfig from '../../Config/audio_config';
import Player from '../Object/Player';
import Platform from '../Object/Platform';
import Parallax from '../Object/Parallax';
import ScoreManager from '../Manager/ScoreManager';

export class GameScene extends Phaser.Scene {
    player: Player;
    platform: Platform;
    cloud: Parallax;
    mountain: Parallax;
    groundCollider: Phaser.Physics.Arcade.Collider;
    interactiveCollider: Phaser.Physics.Arcade.Collider;
    score: ScoreManager;

    //Texts
    gameOverText: Phaser.GameObjects.Text;
    restartText: Phaser.GameObjects.Text;
    jumpText: Phaser.GameObjects.Text;
    crouchText: Phaser.GameObjects.Text;
    fpsText: Phaser.GameObjects.Text;

    constructor() {
        super({
            key: GameSceneKey
        });
    }

    preload(): void {
        
    }

    create(): void {
        this.initializeObjects();
        this.initializeTexts();
        this.initializeControl();
        this.initializePhysics();
        this.initializeSounds();
    }

    initializeSounds(): void {
        this.sound.add(AudioConfig.jump.name);
        this.sound.add(AudioConfig.coin.name);
        this.sound.add(AudioConfig.obstacle.name);
    }

    initializeObjects(): void {
        const width = this.cameras.main.width;
        const height = this.cameras.main.height;

        //Generate player
        this.player = new Player({x: width / 4, y: height / 3}, this.physics);

        //Generate platform
        this.platform = new Platform(this);
        this.platform.generateBricks({width: width, height: height});

        //Generate parallax
        this.cloud = new Parallax(this, ParallaxSpriteConfig.cloud.name, 
            {x: ParallaxSpriteConfig.cloud.dimension.x / 2, y: ParallaxSpriteConfig.cloud.dimension.y / 4},
            ParallaxSpriteConfig.cloud.dimension, DepthConfig.cloud, 0.5, ParallaxConfig.cloud.initialVelocity);
    
        this.mountain = new Parallax(this, ParallaxSpriteConfig.mountain.name,
            {x: ParallaxSpriteConfig.mountain.dimension.x / 2, y: ParallaxSpriteConfig.mountain.dimension.y * 2 / 3},
            ParallaxSpriteConfig.mountain.dimension, DepthConfig.mountain, 1, ParallaxConfig.mountain.initialVelocity);
    
        //Score manager & text
        this.score = ScoreManager.getInstance();
        this.score.setText(this.add.text(TextConfig.score.position.x, TextConfig.score.position.y, this.score.getScore() + '', TextConfig.score.style));
        this.score.start(this); //Starts incrementing score for staying alive
    }

    initializeTexts(): void {
        //Game over texts
        this.gameOverText = this.add.text(TextConfig.gameOver.position.x, TextConfig.gameOver.position.y, 'GAME OVER', TextConfig.gameOver.style);
        this.restartText = this.add.text(TextConfig.restart.position.x, TextConfig.restart.position.y, 'R - Restart', TextConfig.restart.style);

        //Centralize game over texts
        this.centralizeText(this.gameOverText);
        this.centralizeText(this.restartText);

        //Hide game over texts
        this.gameOverText.setVisible(false);
        this.restartText.setVisible(false);

        //Jump & crouch texts
        this.jumpText = this.add.text(TextConfig.jump.position.x, TextConfig.jump.position.y, 'Up Arrow - Jump', TextConfig.jump.style);
        this.crouchText = this.add.text(TextConfig.crouch.position.x, TextConfig.crouch.position.y, 'Down Arrow - Duck', TextConfig.crouch.style);
    
        //Fps text
        this.fpsText = this.add.text(TextConfig.fps.position.x, TextConfig.fps.position.y, 'fps: ' + Math.round(this.game.loop.actualFps), TextConfig.fps.style);
        
        //Update fps meter every second
        this.time.addEvent({
            delay: 1000,
            callback: function () {
                this.fpsText.setText('fps: ' + Math.round(this.game.loop.actualFps))
            }.bind(this),
            loop: true
        });
    }

    //Align center for texts
    centralizeText(text: Phaser.GameObjects.Text): void {
        text.x -= text.width / 2;
        text.y -= text.height / 2;
    }

    initializePhysics(): void {
        //Player's interaction with platform and interactive objects
        this.groundCollider = this.physics.add.collider(this.player.sprite, this.platform.platformCollider, this.playerLanding, null, this);
        this.interactiveCollider = this.physics.add.overlap(this.player.sprite, this.platform.interactiveGroup, this.playerTouching, null, this);
    }

    initializeControl(): void {
        //Player's reaction on cursor press
        const up = this.input.keyboard.addKey('UP');
        up.on('down', function (event) { 
            this.player.jump(this);
        }.bind(this));

        const down = this.input.keyboard.addKey('DOWN');
        down.on('down', function (event) {
            this.player.crouch(this);
        }.bind(this));

        //Restart scene
        const r_key = this.input.keyboard.addKey('R');
        r_key.on('down', function (event) {
            this.destroyObjects();
            this.scene.restart();
        }.bind(this));
    }

    destroyObjects(): void {
        //Remove key listeners
        this.input.keyboard.removeKey('UP');
        this.input.keyboard.removeKey('DOWN');
        this.input.keyboard.removeKey('R');
    }

    playerLanding(player: Phaser.Physics.Arcade.Sprite, ground: Phaser.Physics.Arcade.Image): void {
        //Callback for when player touches platform
        this.player.touchGround();
    }

    playerTouching(player: Phaser.Physics.Arcade.Sprite, interactive: Phaser.Physics.Arcade.Image): void {
        const type: string = interactive.getData('type');
        if (type == InteractiveType.obstacle)
        {
            this.sound.play(AudioConfig.obstacle.name);
            this.gameOver();
        }
        else if (type == InteractiveType.coin)
        {
            //Disable collision
            interactive.body.checkCollision.none = true;

            //Disable update
            interactive.setActive(false);

            //Hides item
            interactive.setVisible(false);

            this.score.addScore(ScoreConfig.coin);

            this.sound.play(AudioConfig.coin.name);
        }
    }

    gameOver(): void {
        this.groundCollider.destroy(); //Removes collider between player & ground
        this.interactiveCollider.destroy(); //Removes player interaction with obstacles and coins
        this.platform.stop(); //Stops platform from moving
        this.mountain.stop(); //Stops mountain from moving
        this.player.kill(); //Kills player
        this.score.stop(); //Stops score survival increment

        this.gameOverText.setVisible(true);
        this.restartText.setVisible(true);
    }

    update(time, delta): void {
        this.platform.update();
        this.cloud.update();
        this.mountain.update();
    }
}