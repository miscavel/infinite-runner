import "phaser";
import { TileSpriteConfig } from '../../Config/sprite_config';
import Platform from "./Platform";
import Interactive from "./Interactive";
import DepthConfig from '../../Config/depth_config';

export default class Brick {
    /*
        A brick is composed of 3 layers, the top ground layer and 2 bedrock layers beneath.
        Player only interacts with the top layer (collider), whereas the remaining layers are solely
        used for visuals.

        'interactive' refers to any interactive object if the brick happens to spawn one.
    */
    top: Phaser.Physics.Arcade.Image;
    mid: Phaser.Physics.Arcade.Image;
    bottom: Phaser.Physics.Arcade.Image;
    interactive: Interactive;

    constructor(x: number, y: number, platform: Platform) {
        this.generateBrick(x, y, platform);
        this.interactive = null;
    }

    generateBrick(x: number, y: number, platform: Platform): void {
        /*
            Generates top ground layer, is set to immovable so that player does not push 
            the platform down.
            It is added to the 'platformCollider' group that interacts with the player.
        */
        this.top = platform.platformCollider.getFirstDead(true, x, y, TileSpriteConfig.name, TileSpriteConfig.tile.ground);
        this.top.setImmovable(true);
        
        /*
            Generates the 2 bedrock layers beneath.
            They are added to the 'platform' group that does not interact with the player.
        */
        this.mid = platform.platform.getFirstDead(true, x, y + TileSpriteConfig.frame.frameHeight, TileSpriteConfig.name, TileSpriteConfig.tile.bedrock);
        this.mid.body.checkCollision.none = true;
        
        this.bottom = platform.platform.getFirstDead(true, x, y + TileSpriteConfig.frame.frameHeight * 2, TileSpriteConfig.name, TileSpriteConfig.tile.bedrock);
        this.bottom.body.checkCollision.none = true;
    
        this.setDepth(DepthConfig.brick);
    }

    setDepth(depth: number) {
        this.top.setDepth(depth);
        this.mid.setDepth(depth);
        this.bottom.setDepth(depth);
    }

    setVelocity(velocity: Phaser.Math.Vector2): void {
        /*
            The rate at which the brick is moving.
            Moving the brick instead of the player makes it easier to recycle the bricks.
        */
        this.top.setVelocity(velocity.x, velocity.y);
        this.mid.setVelocity(velocity.x, velocity.y);
        this.bottom.setVelocity(velocity.x, velocity.y);
    }

    reposition(x: number, y: number, interactiveArr: Array<Interactive>): void {
        /*
            Relocates the brick to another position
        */
        this.top.x = x;
        this.top.y = y;

        this.mid.x = x;
        this.mid.y = y + TileSpriteConfig.frame.frameHeight;

        this.bottom.x = x;
        this.bottom.y = y + TileSpriteConfig.frame.frameHeight * 2;

        this.disableInteractive(interactiveArr);
    }

    disableInteractive(interactiveArr: Array<Interactive>): void {
        //If the brick has an interactive object on top of it
        if (this.interactive != null) {
            //Disable the interactive object attached to the brick to be recycled
            this.interactive.disable();
            
            //Return interactive object to the pool
            interactiveArr.push(this.interactive);

            //Release interactive object as it is no longer bound to the brick
            this.interactive = null;
        }
    }

    //Attach interactive to brick
    setInteractive(interactive: Interactive): void {
        this.interactive = interactive;
        interactive.brick = this;
    }
}