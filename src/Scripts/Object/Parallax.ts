import "phaser";
import { ParallaxSpriteConfig } from '../../Config/sprite_config';
import ParallaxConfig from '../../Config/parallax_config';

export default class Parallax {
    images: Array<Phaser.Physics.Arcade.Image>;
    dimension: {x: number, y: number};
    boundary: number;
    lastImage: Phaser.Physics.Arcade.Image;

    constructor(scene: Phaser.Scene, name: string, position: {x: number, y: number}, dimension: {x: number, y: number}, depth: number, alpha: number, velocity: Phaser.Math.Vector2) {
        this.images = [];
        this.dimension = dimension;
        this.boundary = -dimension.x;
        this.generateImages(scene, name, position, depth, alpha, velocity);
    }

    update(): void {
        this.recycleImages();
    }

    generateImages(scene: Phaser.Scene, name: string, position: {x: number, y: number}, depth: number, alpha: number, velocity: Phaser.Math.Vector2) {
        let image: Phaser.Physics.Arcade.Image;
        
        //Generate images up to the number set in the config
        for (let index = 0; index < ParallaxConfig.imgCount; index++) {
            image = this.generateImage(scene, name, {x: position.x + this.dimension.x * index, y: position.y}, depth, alpha, velocity);
            this.images.push(image);
        }
        this.lastImage = this.images[this.images.length - 1];
    }

    generateImage(scene: Phaser.Scene, name: string, position: {x: number, y: number}, depth: number, alpha: number, velocity: Phaser.Math.Vector2): Phaser.Physics.Arcade.Image {
        const image: Phaser.Physics.Arcade.Image = scene.physics.add.image(position.x, position.y, name);
        image.body.checkCollision.none = true;
        image.setDepth(depth);
        image.setAlpha(alpha);
        image.setVelocity(velocity.x, velocity.y);
        return image;
    }

    recycleImages(): void {
        //Function to reposition images that are out of view
        this.images.forEach(function (image: Phaser.Physics.Arcade.Image) {
            //An image which 'x' value is below the boundary is repositioned
            if (image.body.position.x < this.boundary) {
                //Reposition image next to the furthest image
                const x = this.lastImage.body.position.x + this.dimension.x * 1.5;
                const y = this.lastImage.body.position.y + this.dimension.y / 2;

                image.setPosition(x, y);
                this.lastImage = image;
            }
        }.bind(this));
    }

    stop(): void {
        this.images.forEach(function (image: Phaser.Physics.Arcade.Image) {
            image.setVelocity(0, 0);
        });
    }
}