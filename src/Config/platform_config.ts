import "phaser";

export default {
    initialCount: 20, //Initial number of bricks to be generated
    initialVelocity: new Phaser.Math.Vector2(-300, 0), //Speed at which platform is moving
    leftBoundary: {x: -32, y: 0} //Boundary which dictates when a brick would be recycled
}