import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from './config';

//Settings for texts
export default {
    score : {
        position: {x: DEFAULT_WIDTH / 2, y: DEFAULT_HEIGHT / 10},
        style: {
            fontSize: '60px',
            fill: '#00000'
        }
    },
    gameOver: {
        position: {x: DEFAULT_WIDTH / 2, y: DEFAULT_HEIGHT * (5 / 12)},
        style: {
            fontSize: '60px',
            fill: '#00000'
        }
    },
    restart: {
        position: {x: DEFAULT_WIDTH / 2, y: DEFAULT_HEIGHT * (5 / 12) + 45 },
        style: {
            fontSize: '30px',
            fill: '#00000'
        }
    },
    jump: {
        position: {x: 20, y: DEFAULT_HEIGHT / 16},
        style: {
            fontSize: '28px',
            fill: '#00000'
        }
    },
    crouch: {
        position: {x: 20, y: DEFAULT_HEIGHT / 16 + 45},
        style: {
            fontSize: '28px',
            fill: '#00000'
        }
    },
    fps: {
        position: {x: 10, y: DEFAULT_HEIGHT / 16 - 40},
        style: {
            fontSize: '28px',
            fill: '#FFFFFF'
        }
    }
}