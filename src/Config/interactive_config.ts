import { TileSpriteConfig } from './sprite_config';

//The types of interactive objects in the game
export const InteractiveType = {
    obstacle: 'obstacle',
    coin: 'coin'
}

export default {
    objects: [
        {
            type: InteractiveType.obstacle, //Dictates the type of the object (for interaction with player)
            index: TileSpriteConfig.tile.fence, //Dictates the index of the object sprite in the spritesheet
            offset: {x: 0, y: -TileSpriteConfig.frame.frameHeight} //Dictates the offset at which the object is spawned, anchored to the given brick tile
        },
        {
            type: InteractiveType.obstacle,
            index: TileSpriteConfig.tile.sun,
            offset: {x: 0, y: -TileSpriteConfig.frame.frameHeight * 2.1}
        },
        {
            type: InteractiveType.coin,
            index: TileSpriteConfig.tile.coin,
            offset: {x: 0, y: -TileSpriteConfig.frame.frameHeight * 2.5}
        }
    ],
    step: 8 //Number of steps to spawn an object
}