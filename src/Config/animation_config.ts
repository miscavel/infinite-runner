export const PlayerAnimationList = {
    running: 'running',
    jumping: 'jumping',
    crouching: 'crouching'
}

export const PlayerAnimationConfig = [
    {
        key: PlayerAnimationList.running,
        frames: [2, 3],
        frameRate: 8,
        repeat: -1
    },
    {
        key:  PlayerAnimationList.jumping,
        frames: [0, 1],
        frameRate: 3,
        repeat: 0
    },
    {
        key:  PlayerAnimationList.crouching,
        frames: [6],
        frameRate: 8,
        repeat: 0
    }
];