export default {
    coin: 100, //Score increment for coin collection
    step: 10, //Score increment for step
    stepDelay: 2000 //Increases score by step after every 2000ms
}